#
#
#
FROM centos:7.1.1503
MAINTAINER "you" <your@email.here>
ENV container docker
ENV centversion 7.1.1503

RUN yum --enablerepo=* clean all
ADD yum.repos.d/Local-*.repo /etc/yum.repos.d/

RUN yum -y --disablerepo=base,updates,extras install yum-utils
RUN yum-config-manager --disable base updates extras pgdg94 epel
RUN yum --enablerepo=* clean all
RUN yum -y --releasever=${centversion} update 

RUN yum -y --releasever=${centversion} swap -- remove fakesystemd -- install systemd systemd-libs
RUN yum -y --releasever=${centversion} update; yum clean all; \
(cd /lib/systemd/system/sysinit.target.wants/; for i in *; do [ $i == systemd-tmpfiles-setup.service ] || rm -f $i; done); \
rm -f /lib/systemd/system/multi-user.target.wants/*;\
rm -f /etc/systemd/system/*.wants/*;\
rm -f /lib/systemd/system/local-fs.target.wants/*; \
rm -f /lib/systemd/system/sockets.target.wants/*udev*; \
rm -f /lib/systemd/system/sockets.target.wants/*initctl*; \
rm -f /lib/systemd/system/basic.target.wants/*;\
rm -f /lib/systemd/system/anaconda.target.wants/*;
VOLUME [ "/sys/fs/cgroup" ]
CMD ["/usr/sbin/init"]
