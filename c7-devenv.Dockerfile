#
#
#
FROM local/c7-systemd
MAINTAINER "you" <your@email.here>
ENV container docker
ENV builduser builduser

COPY yum.repos.d/LiveMedia.repo /etc/yum.repos.d/
RUN yum --enablerepo=* clean all

RUN yum -y --releasever=${centversion} groups install \
	"Base" "Development Tools"

RUN yum -y --releasever=${centversion} install \
	java-1.8.0-openjdk java-1.8.0-openjdk-devel \
	java-1.8.0-openjdk-headless javapackages-tools

RUN yum -y --releasever=${centversion} install \
	rpmdevtools rpm-build \
	xmlto asciidoc hmaccalc python-devel newt-devel \
	perl-ExtUtils-Embed pesign elfutils-devel zlib-devel \
	binutils-devel audit-libs-devel numactl-devel \
	pciutils-devel ncurses-devel

RUN yum -y --releasever=${centversion} install \
	createrepo wget curl lftp

RUN yum -y --releasever=${centversion} install livecd-tools
RUN yum list installed

RUN useradd -m -c "build account" -d /home/${builduser} -s /bin/bash ${builduser}


WORKDIR /home/${builduser}
USER ${builduser}

RUN rpmdev-setuptree

CMD ["/bin/bash"]
